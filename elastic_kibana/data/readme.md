
Create index 

```shell
delete log1_journal

PUT log1_journal
{
  "mappings": {
    "properties": {
      "creationdate": {
        "type": "date",
        "format": "yyyyMMddHHmmss" 
      },
      "svipath": {
        "type": "text", 
        "analyzer": "breadcrumb",
        "fielddata": true
      }
    }
  },
  "settings": {
    "analysis": {
      "tokenizer": {
        "breadcrumb_path_hierarchy": {
          "type": "path_hierarchy"
        }
      },
      "analyzer": {
        "breadcrumb": {
          "type": "custom",
          "tokenizer": "breadcrumb_path_hierarchy",
          "filter" : ["trim"]
        }
      }
    }
  }
}

### Other possible configuration

delete log_journal

PUT log_journal
{
  "mappings": {
    "properties": {
      "creationdate": {
        "type": "date",
         "format": "yyyy-MM-dd HH:mm:ss.SSSSSS||yyyy-MM-dd HH:mm:ss.SSS||yyyy-MM-dd HH:mm:ss||epoch_millis"
      },
      "svipath": {
        "type": "text",
        "fields" : {
          "tree": {
            "type":"text",
            "analyzer":"custom_path_tree",
            "fielddata": true
          },
          "tree_reversed": {
            "type": "text",
            "analyzer": "custom_path_tree_reversed",
            "fielddata": true
          },
          "key": {
            "type": "text",
            "analyzer": "custom_keyword_analyzer",
             "fielddata": true
          }
        }
      }, 
      "Resultat": {
        "type": "keyword"
      },
      "ConnId": {
        "type": "keyword"
      },
      "Appele": {
        "type": "keyword"
      },
      "Appelant": {
        "type": "keyword"
      },
      "RAddr": {
        "type": "keyword"
      },
      "Code_result": {
        "type": "text",
      },
      "Choix_niv1": {
        "type": "byte",
      },
      "Choix_niv2": {
        "type": "byte",
      },
      "Choix_niv3": {
        "type": "byte",
      },
      "Choix_niv4": {
        "type": "byte",
      },
      "Niv_sortie": {
        "type": "byte",
      },
      "Err_niv1": {
        "type": "byte",
      },
      "Err_niv2": {
        "type": "byte",
      },
      "Err_niv3": {
        "type": "byte",
      },
      "Err_niv4": {
        "type": "byte",
      },
      "WS": {
        "type": "text"
      }
    }
  },
  "settings": {
    "analysis": {
      "tokenizer": {
        "breadcrumb_path_hierarchy": {
          "type": "path_hierarchy"
        },
        "custom_hierarchy": {
          "type": "path_hierarchy",
          "delimiter": "/"
        },
        "custom_hierarchy_reversed": {
          "type": "path_hierarchy",
          "delimiter": "/",
          "reverse": "true"
        },
       "custom_keyword_tokenizer": {
          "type": "pattern",
          "pattern": "/"
        }
      },
      "analyzer": {
        "breadcrumb": {
          "type": "custom",
          "tokenizer": "breadcrumb_path_hierarchy",
          "filter" : ["trim"]
        },
        "custom_path_tree": {
          "tokenizer": "custom_hierarchy"
        },
        "custom_path_tree_reversed": {
          "tokenizer": "custom_hierarchy_reversed"
        },
        "custom_keyword_analyzer": {
          "tokenizer": "custom_keyword_tokenizer"
        }
      }
    }
  }
}

POST log2_journal/_doc 
    { "creationdate" : "20210129213605",
      "svipath" : "PAR/0.SVI/1.QUALIF/2.SINISTRE/3.AUTO", "name" : "log14" 
      
    }

GET log_journal/_search
{
 "aggs": {             
    "byCategory": {             
      "terms": {                        
        "field": "svipath.tree",
        "size": 2000
      }      
    }
  },
  "size": 0               
}

```

docker run --rm -ti -v $(pwd):/import docker.elastic.co/logstash/logstash:7.16.2 logstash -f /import/data/log_journal.conf